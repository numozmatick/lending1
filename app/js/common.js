//Слайдер
$(document).ready(function () {
    $('.slider').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
    })
//Клики
    $('.menu').on('click', function () {
        let nav = $('nav');
        nav.addClass('active');
        $('.overlay').css('display', 'block');
    })
    $('.menu__item').on('click', function () {
        $('nav').removeClass('active');
        $('.overlay').css('display', 'none')
    })
    $('.exit').on('click', function () {
        $('nav').removeClass('active');
        $('.overlay').css('display', 'none')
    })
    let pages = ["resume", "advantage", "result", "work", "tariff", "form-communication", "link"];
    pages.forEach(el => {
        $('.' + el + 'Link').click(function () {
            var next = $('.' + el)
            $("html, body").animate(
                {
                    scrollTop: next.offset().top + parseInt($('section').css('padding-top')) / 1.3
                }, 500
            );
        });
    })
    $('.btn--tariff').click(function () {
        var next = $('.form-communication')
        $("html, body").animate(
            {
                scrollTop: next.offset().top + parseInt($('section').css('padding-top'))
            }, 500
        );
    });
    //Форма
    $(".form").submit(function () {
        $.ajax({
            type: "POST",
            url: "mail.php",
            data: $(this).serialize()
        }).done(function () {
            $(this).find("input").val("");
            alert("Спасибо за заявку! Скоро мы с вами свяжемся.");
            $(".form").trigger("reset");
        });
        return false;
    });
//lazy
    $(function () {
        $('.lazy').lazy({
            scrollDirection: 'vertical',
            effect: 'fadeIn',
            visibleOnly: true,
        });
    });

    //mask
    $(".phone").mask("+7 (999) 999-99-99");
})


